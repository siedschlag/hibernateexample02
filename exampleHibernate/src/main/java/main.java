
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;


import org.hibernate.cfg.Configuration;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;

import javax.persistence.Entity;
import javax.persistence.Table;


import java.util.ArrayList;
import java.util.List;

public class main {



    public static void main(String args[]){


        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("org.hibernate.tutorial.jpa");

        Cliente cliente=new Cliente();
        cliente.setName("Joao");

        Bank bank = new Bank();
        bank.setName("Itau");
        bank.setId(5);

        List<Bank> Banks_lista = new ArrayList<Bank>();
        Banks_lista.add(bank);
        //adicionando lista de bancos a clientes
        cliente.setBank(Banks_lista);

        EntityManager entityManager = entityManagerFactory.createEntityManager();
        entityManager.getTransaction().begin();

        entityManager.merge(cliente);//persist quando esta criando
        entityManager.getTransaction().commit();



        SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        session.beginTransaction();
//        Query query =  session.createQuery("select idade FROM Bank ");
//        List bank = query.list();
//        session.getTransaction().commit();
//        session.close();
//        System.out.println("tamanho "+bank.size());
        String hql = "select name  from Bank";
        Query query = session.createQuery(hql);
        //query.setDouble("preco",25.0);
        List results = query.list();
        System.out.println(results);


    }
}
