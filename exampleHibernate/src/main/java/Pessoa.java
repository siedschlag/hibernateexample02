import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


@Entity
@Table(name="pessoa_table")
public class Pessoa implements Serializable {
    private String nome;

    private List<Projeto> projeto = new ArrayList<Projeto>();


@Id
@GeneratedValue(strategy = GenerationType.AUTO)
@Column(name="oid_pessoa")
private int id_pessoa;

//@OneToMany(mappedBy ="pessoa" , orphanRemoval = true)
//@Cascade({org.hibernate.annotations.CascadeType.ALL})


//muitos para muitos cria uma tabela no meio entre as duas
@ManyToMany(cascade = CascadeType.ALL)
@JoinTable(name="pessoa_has_projetos",
        joinColumns={@JoinColumn(name="pessoa_id", referencedColumnName="id_pessoa")},
        inverseJoinColumns={@JoinColumn(name="projeto_id", referencedColumnName="id_projeto")})
    //get e setters


    public int getId_pessoa() {
        return id_pessoa;
    }

    public void setId_pessoa(int id_pessoa) {
        this.id_pessoa = id_pessoa;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }











}
