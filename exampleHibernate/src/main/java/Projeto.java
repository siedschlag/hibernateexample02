import org.hibernate.annotations.ManyToAny;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Projeto implements Serializable {
    private List<Pessoa> pessoas = new ArrayList<Pessoa>();

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id_projeto")
    private long id_projeto;
    @Column(name="nomeProjeto")
    private String nomeProjeto;

    //o mapeamento de pessoas vai para o lado projetos
    @ManyToMany(mappedBy = "projetos",cascade = CascadeType.ALL)



    public long getId_projeto() {
        return id_projeto;
    }

    public void setId_projeto(long id_projeto) {
        this.id_projeto = id_projeto;
    }
    public String getNomeProjeto() {
        return nomeProjeto;
    }

    public void setNomeProjeto(String nomeProjeto) {
        this.nomeProjeto = nomeProjeto;
    }

    public List<Pessoa> getPessoas() {
        return pessoas;
    }

    public void setPessoas(List<Pessoa> pessoas) {
        this.pessoas = pessoas;
    }
}
