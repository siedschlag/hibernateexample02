import java.util.ArrayList;
import java.util.List;

public class main02 {

    public static void main02(String args[]){

        Pessoa pessoa = new Pessoa();
        pessoa.setNome("Leonardo");
        Projeto projeto= new Projeto();
        projeto.setNomeProjeto("Construção Ponte Ercilio Luz");

        List<Pessoa> pessoas = new ArrayList<Pessoa>();
        pessoas.add(pessoa);

        projeto.setPessoas(pessoas);


    }

}
