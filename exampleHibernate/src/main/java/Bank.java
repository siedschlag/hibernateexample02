import org.hibernate.annotations.GenericGenerator;
import sun.misc.Cleaner;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name= "banks_table")
public class Bank implements Serializable {
    @Id
    private int id_bank;
    private List clientes;


    public int getId() {
        return id_bank;
    }
    public void setId(int id_bank) {
        this.id_bank = id_bank;
    }

    @Column(name="name")
    private String name;
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Column(name="idade")
    private String idade;
    @ManyToMany(mappedBy="bank",targetEntity = Cliente.class,cascade = CascadeType.ALL)


    public String getIdade() {
        return idade;
    }

    public void setIdade(String idade) {
        this.idade = idade;
    }




}
