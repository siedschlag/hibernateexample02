import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="client_table")
public class Cliente implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name="id_cliente")
    private Integer id_cliente;

    @Column(name="name")
    private String name;



    @OneToMany(cascade = CascadeType.ALL)
    //@JoinTable nome da tabela que vai ser pq e uma relação n para n, joinColumns->lado dominante, inverseJoinColumns->lado dominado
    @JoinColumn(name="cliente_has_bank", nullable=false, insertable=true, updatable=true)
    private List<Bank> bank = new ArrayList<Bank>();

    public String getName(){
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getId_cliente() {
        return id_cliente;
    }

    public void setId(Integer id_cliente) {
        this.id_cliente = id_cliente;
    }
    public List getBank() {
        return bank;
    }

    public void setBank(List bank) {
        this.bank = bank;
    }


}
